import React from 'react';
import clsx from 'clsx';
import { Link } from 'react-router-dom';
import { firebaseDB } from '../../Firebase';
import CustomStyle from '../../modules/Home.module.css';

import {
    Card,
    CardHeader,
    CardMedia,
    CardContent,
    CardActions,
    Avatar,
    Collapse,
    IconButton,
    Typography,
    Tooltip,
    Menu,
    MenuItem
} from '@material-ui/core';

import {
    Favorite as FavoriteIcon,
    Share as ShareIcon,
    ExpandMore as ExpandMoreIcon,
    MoreVert as MoreVertIcon
} from '@material-ui/icons';

import { makeStyles } from '@material-ui/core/styles';
import ConfirmDialouge from '../widgets/Confirmation';
import PostMomentTime from './MomentTime';


// Stylesheet for this Component..
const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        marginBottom: '1rem'
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: 'red',
    },
}));


// Components Function..
const PostCard = ({ postData, postAdminData, isProfile, customStyles }) => {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [actionAgreement, setActionAgreement] = React.useState({
        open: false,
        agreement: false
    });

    // console.log(PostMomentTime);

    // console.log('PostAdmin Data here ==== ', postAdminData);

    // Check Is On Profile then show..
    // console.log('==== inner props -->> ',isProfile);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    // To Delete Post..
    const deletePost = () => {
        // console.log('Clicked For ', postId);
        setAnchorEl(null);

        // ActionAgreement Setting..
        setActionAgreement({...actionAgreement, open: true});
    };  

    // console.log('===== ',actionAgreement);

    // Make Agreement and delete post when confirm on dialouge box..
    const makeDeleteWithActionAgreement = () => {
            firebaseDB.ref(`posts/${postData.id}`).remove();
            // To Reload page with Action..
            window.location.reload();
    };

    if(actionAgreement.agreement){
        makeDeleteWithActionAgreement();
    }


    // Components Return statement..
    return (
        <Card className={clsx(classes.root, customStyles ? customStyles : null)}>
            <CardHeader
                avatar={
                    <Avatar aria-label="recipe" className={classes.avatar}>
                        <img src={postAdminData.adminAvatar} alt="AdminURL" />
                    </Avatar>
                }
                action={
                    isProfile && (
                        <IconButton aria-label="settings" onClick={handleClick}>
                            <Tooltip title="Actions">
                                <MoreVertIcon />
                            </Tooltip>
                        </IconButton>
                    )
                }
                title={postAdminData.displayAdminName}
                subheader={<PostMomentTime time={postData.time} />}
            />

            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem onClick={handleClose}>Edit Post</MenuItem>
                <MenuItem onClick={deletePost}>Delete Post</MenuItem>
            </Menu>

            {/*----- Action Agreement Modal To Make Action Surely -----*/}
            { actionAgreement && 
                    <ConfirmDialouge 
                        confirmation={actionAgreement} 
                        setConfirm={setActionAgreement}
                    /> 
            }

            {/*---- Post Image ----*/}
            <Link to={`post/${postData.id}`}>
                <CardMedia
                    className={classes.media}
                    image={postData.image}
                    title={postData.title}
                />
            </Link>

            {/*------ Post Title Area ------*/}
            <CardContent>
                <Typography variant="body2" color="textSecondary" component="p">
                    <Link to={`/post/${postData.id}`} className={CustomStyle.postTitleOnCard}>
                        {postData.title}
                    </Link>
                </Typography>
            </CardContent>

            <CardActions disableSpacing>
                <IconButton aria-label="add to favorites">
                    <FavoriteIcon />
                </IconButton>
                <IconButton aria-label="share">
                    <ShareIcon />
                </IconButton>
                <IconButton
                    className={clsx(classes.expand, {
                        [classes.expandOpen]: expanded,
                    })}
                    onClick={() => setExpanded(!expanded)}
                    aria-expanded={expanded}
                    aria-label="show more"
                >
                    <ExpandMoreIcon />
                </IconButton>
            </CardActions>


            {/*------ Post Body Area -----*/}
            <Collapse in={expanded} timeout="auto" unmountOnExit>
                <CardContent>
                    <Typography paragraph>Method:</Typography>
                    <Typography paragraph>
                        {postData.title}
                    </Typography>

                    <Typography paragraph>
                        <p
                            dangerouslySetInnerHTML={{
                                __html: postData.content
                            }}
                        >
                        </p>
                    </Typography>

                    <Typography>
                        This User Last Signed In : {postAdminData.lastSignIn}
                    </Typography>
                </CardContent>
            </Collapse>
        </Card>
    )
}

export default PostCard;