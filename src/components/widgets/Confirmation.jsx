import React from 'react';
import {
    Button,
    Dialog,
    DialogActions,
    DialogTitle,
    useMediaQuery
} from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';


// Export Only Module Component here..
export default function ResponsiveDialog({ confirmation, setConfirm }) {
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    // Page Returning Statement..
    return (
        <>
            <Dialog
                fullScreen={fullScreen}
                open={confirmation.open}
                onClose={ () => setConfirm({...confirmation, open: true}) }
                aria-labelledby="responsive-dialog-title"
            >
                <DialogTitle id="responsive-dialog-title">{"Are you sure wants to delete it forever?"}</DialogTitle>
                <DialogActions>
                    <Button autoFocus onClick={ () => setConfirm({agreement: false, open: false}) } color="primary">
                        cancel
                    </Button>
                    <Button onClick={ () => setConfirm({agreement: true, open: false}) } color="primary" autoFocus>
                        ok
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
}
