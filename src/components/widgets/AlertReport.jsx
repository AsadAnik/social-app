import React from 'react';
import { Alert, AlertTitle } from '@material-ui/lab';

//Alert Reporter Component..
const AlertReport = ({severityType, alertText, customStyle}) => {
    switch (severityType) {
        case "error":
            return (
                <Alert severity={severityType} className={customStyle}>
                    <AlertTitle>Error</AlertTitle>
                    <strong>{alertText}</strong>
                </Alert>
            );
        
        case "success":
            return (
                <Alert severity={severityType} className={customStyle}>
                    <AlertTitle>Success</AlertTitle>
                    <strong>{alertText}</strong>
                </Alert>
            );
    
        default:
            break;
    }
}

export default AlertReport;