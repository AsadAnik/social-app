import React from 'react';
import { PropagateLoader } from 'react-spinners';
import { css } from '@emotion/core';
import {
    CircularProgress,
    LinearProgress,
} from '@material-ui/core';


const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
    position: absolute;
    left: 50%;
    top: 40%;
    transform: translate(-50%, -50%);
`;


// The Spinner Component..
const Spinner = ({ size, color, loaderCondition }) => {
    return (
        <PropagateLoader
            css={override}
            size={size}
            color={color}
            loading={loaderCondition}
        />
    );
}


// The Circular Component..
const CircularLoader = ({ customStyle }) => {
    return <CircularProgress className={customStyle} />;
}


// Progress FileUpload loading..
const LinearLoader = () => {
    const [progress, setProgress] = React.useState(0);
    const [buffer, setBuffer] = React.useState(10);
    
    // Use Ref For Progress Ref..
    const progressRef = React.useRef(() => { });

    // React Hook For Counting on loading..
    React.useEffect(() => {
        progressRef.current = () => {
            if (progress > 100) {
                setProgress(0);
                setBuffer(10);
            } else {
                const diff = Math.random() * 10;
                const diff2 = Math.random() * 10;
                setProgress(progress + diff);
                setBuffer(progress + diff + diff2);
            }
        };
    });

    // Use Hook For Set Time on this Loader..
    React.useEffect(() => {
        const timer = setInterval(() => {
            progressRef.current();
        }, 500);

        return () => {
            clearInterval(timer);
        };
    }, []);


    // Returning the Loader...
    return <LinearProgress variant="buffer" value={progress} valueBuffer={buffer} />
}



export {
    Spinner,
    CircularLoader,
    LinearLoader
};