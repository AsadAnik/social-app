import React from 'react';
import Moment from 'react-moment';

const MomentTime = ({ time }) => (
    <Moment>{time}</Moment>
)

export default MomentTime;