import React from 'react';
import UsersList from './usersList';
import AppColor from './appColor';
// Firebase..
import { usersDatabase, firebaseLooper } from '../../Firebase';

// Setting Components Function..
const Settings = () => {
    const [usersListData, setUserslistData] = React.useState([]);

    // React Hook..
    React.useEffect(() => {
        usersDataToList();
    }, ['']);

    // All Users List for Settings Show All users from Database..
    const usersDataToList = () => {
        usersDatabase.once('value')
            .then(snapShot => {
                const snapData = firebaseLooper(snapShot);
                setUserslistData(snapData);
            })
            .catch(error => console.log('Error UserListData : ', error.message));
    }

    // console.log('Users Data ----- ', usersListData);

    // Settings Returning Statement..
    return (
        <>
            {/* Users List Item */}
            <h3>All Users List</h3>
            <UsersList allUsers={usersListData} />

            {/* App Theme Background */}
            <h3>Application Theme Colors</h3>
            <AppColor user={usersListData} />

            {/* Application Theme Mod */}
            <h3>Background Mod!</h3>
        </>
    )
}

export default Settings;