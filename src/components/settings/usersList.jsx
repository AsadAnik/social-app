import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Paper, List, ListItem, ListItemText, ListItemAvatar, Avatar, Typography } from '@material-ui/core';
import { LinearLoader } from '../widgets/SpinnerLoading';

// Styles Component for CSS Material UI...
const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
        borderBottom: '1px solid lightgray'
    },
    inline: {
        display: 'inline',
    },
}));


// Users List Components Function..
const UsersList = ({ allUsers }) => {
    const classes = useStyles();

    // Generating the Users All List from Database...
    const allUsersGenerator = (users) => (
        users.length ?
            users.map((item, key) => (
                <List key={key} className={classes.root}>
                <ListItem alignItems="flex-start">
                    <ListItemAvatar>
                        <Avatar alt={item.displayName} src={item.userAvatar} />
                    </ListItemAvatar>
                    <ListItemText
                        primary={`${item.displayName} (${item.username})`}
                        secondary={
                            <React.Fragment>
                                <Typography
                                    component="span"
                                    variant="body2"
                                    className={classes.inline}
                                    color="textPrimary"
                                >
                                    { item.email + " " }
                                </Typography>
                                { item.lastSignInTime }
                            </React.Fragment>
                        }
                    />
                </ListItem>

                {/* <Divider variant="inset" component="li" /> */}
            </List>
            ))
            :
            <LinearLoader />
    );

    // Returning Component..
    return (
        <Paper>
            { allUsersGenerator(allUsers)}
        </Paper>
    );
}

export default UsersList