import React from 'react';
import clsx from 'clsx';
import { Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import customStyle from '../../modules/Setting.module.css';

// Recoil Store..
import { useRecoilState } from 'recoil';
import { profileUserEditState } from '../../RecoilStore/RecoilAtom';

import { firebaseDB, firebase } from '../../Firebase';

const useStyle = makeStyles(() => ({
    colorSchameRoot: {
        display: 'inline-flex',
    },

    colorSchame: {
        padding: '1rem',
        width: '1rem',
        height: '1rem',
        borderRadius: '100%',
        listStyleType: 'none',
        marginRight: '1rem',
        border: '1px solid lightgray'
    },
}));


// AppColorSchame Components Function...
const AppColor = ({user}) => {
    const classes = useStyle();

    // Recoil hook..
    const [profileUserData, setProfileUserData] = useRecoilState(profileUserEditState);
    // console.log(profileUserData);


    // Color Schema Chnage Function..
    const colorSchema = (event) => {
        let value = event.target.value;
        // console.log(value);
        setProfileUserData({...profileUserData, appThemeColor: value});

        // need to database update for appThemeColor set..
    }



    // Returning Statement Of ColorSchame..
    return (
        <Paper>
            <ul className={classes.colorSchameRoot}>
                <button
                    className={clsx(classes.colorSchame, customStyle.schameSectionLi)}
                    style={{ background: 'red' }}
                    value="red"
                    onClick={(event) => colorSchema(event)}
                ></button>

                <button
                    className={clsx(classes.colorSchame, customStyle.schameSectionLi)}
                    style={{ background: 'blue' }}
                    value="blue"
                    onClick={(event) => colorSchema(event)}
                ></button>

                <button
                    className={clsx(classes.colorSchame, customStyle.schameSectionLi)}
                    style={{ background: 'yellow' }}
                    value="yellow"
                    onClick={(event) => colorSchema(event)}
                ></button>

                <button
                    className={clsx(classes.colorSchame, customStyle.schameSectionLi)}
                    style={{ background: 'green' }}
                    value="green"
                    onClick={(event) => colorSchema(event)}
                ></button>

                <button
                    className={clsx(classes.colorSchame, customStyle.schameSectionLi)}
                    style={{ background: 'purple' }}
                    value="purple"
                    onClick={(event) => colorSchema(event)}
                ></button>

                <button
                    className={clsx(classes.colorSchame, customStyle.schameSectionLi)}
                    style={{ background: 'royalblue' }}
                    value="royalblue"
                    onClick={(event) => colorSchema(event)}
                ></button>

                <button
                    className={clsx(classes.colorSchame, customStyle.schameSectionLi)}
                    style={{ background: 'gray' }}
                    value="gray"
                    onClick={(event) => colorSchema(event)}
                ></button>

                <button
                    className={clsx(classes.colorSchame, customStyle.schameSectionLi)}
                    style={{ background: 'black' }}
                    value="black"
                    onClick={(event) => colorSchema(event)}
                ></button>
            </ul>
        </Paper>
    );
}


export default AppColor;