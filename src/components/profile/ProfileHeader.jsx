import React from 'react';
import clsx from 'clsx';
import { Paper, Grid, ButtonBase, Typography, Icon, Tooltip } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Styles from '../../modules/Profile.module.css';
import { Edit as EditIcon, CheckCircle as VerifiedIcon } from '@material-ui/icons';

import { useRecoilState } from 'recoil';
import { profileUserEditState } from '../../RecoilStore/RecoilAtom';

//Styles of classess..
const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }
}));


///Components Function...
const ProfileHeader = ({ username, edit, editBar, setEdit, userInfo }) => {
    const classes = useStyles();
    // Recoil State...
    const [globalUser, setGlobalUser] = useRecoilState(profileUserEditState); 

    // console.log(userInfo);
    // console.log(profileAction);
    const { displayName, photoURL, email, emailVerified, metadata } = userInfo;
    const accountCreationTime = metadata.creationTime;

    // making the username from realname here...
    const makeUsername = (realname) => {
        let username = realname.toLowerCase().split(' ');
        return username[0].concat(username[1]);;
    }

    // React Hook..
    React.useEffect(() => {
        const userForGlobal = makeUsername(displayName);
        setGlobalUser({...globalUser, username: userForGlobal});
    }, [displayName]);


    //Rendering Everythings of profile Header Area with condition wise..
    const renderProfileDetail = (prevCondition, nextCondition, type, edit) => {
        switch (type) {
            case "username":
                return (
                    <h1 className={classes.profileName}>
                        {!edit ?
                            (nextCondition ? nextCondition : prevCondition)
                            :
                            editBar((nextCondition ? nextCondition : prevCondition), 'Edit Profile', 'profileName')}
                    </h1>
                );

            case "nickname":
                return (
                    !nextCondition ?
                        <span>({prevCondition.toLowerCase().split(' ')})</span>
                        :
                        <span>({nextCondition.toLowerCase().split(' ')})</span>
                );

            case "email":
                return (
                    nextCondition && <p>Email: {nextCondition}</p>
                );

            case "verifiedEmail":
                return (
                    nextCondition && <Icon><VerifiedIcon style={{ color: 'royalblue' }} /></Icon>
                );

            case "profileAvatar":
                return (
                    <img
                        className={Styles.profileAvatar}
                        alt="complex"
                        src={!nextCondition ? prevCondition : nextCondition}
                    />
                );

            default:
                return null;
        }
    }

    // Only Not Verified Email Accounts can change the usernames..
    const showEditButton = (verified) => (
        !verified && (
            <Tooltip title="Edit">
                <EditIcon
                    className={Styles.editButton}
                    onClick={() => !edit ? setEdit({ edit: true }) : setEdit({ edit: false })}
                />
            </Tooltip>
        )
    );


    //The Return Statement of this Component..
    return (
        <Paper className={classes.paper}>
            <Grid container spacing={2}>
                {/*----- Profile Image/Avatar And Edit Avatar System -----*/}
                <Grid item>
                    <ButtonBase className={clsx(classes.image, Styles.profileAvatar)}>
                        {/*----- Profile Image Rendering Function -----*/}
                        {renderProfileDetail("images/profile-avatar.jpg", photoURL, "profileAvatar", false)}

                        {/*----- Edit Profile Image Button -----*/}
                        {/* <div className={clsx(classes.fileupload, Styles.profileAvatarEdit)}>
                            <input type="file" className={classes.input} />
                            <EditIcon />
                        </div> */}
                    </ButtonBase>
                </Grid>

                {/*----- Profile Name And Edit Them -----*/}
                <Grid item xs={6} sm container>
                    <Grid item xs container direction="column" spacing={2}>
                        <Grid item xs className={Styles.profileInfoSection}>
                            {/*-------- Profile Name --------*/}
                            <Typography gutterBottom variant="subtitle1">
                                {renderProfileDetail(username, displayName, "username", edit)}
                                {renderProfileDetail(username, displayName, "nickname", edit)}
                            </Typography>

                            {/*------ Account Creation time here -----*/}
                            <Typography gutterBottom variant="caption">
                                { accountCreationTime && <>Created On: { accountCreationTime }</> }
                            </Typography>

                            {/*---- Username ----*/}
                            <Typography variant="body2" color="textSecondary">
                                {renderProfileDetail(false, email, "email", false)}
                            </Typography>

                            {/*-------  Edit Button -------*/}
                            {showEditButton(emailVerified)}
                        </Grid>
                    </Grid>
                </Grid>

                {/*----- Check if Verified user with his email account -----*/}
                <Grid item>
                    {renderProfileDetail(false, emailVerified, "verifiedEmail", false)}
                </Grid>
            </Grid>
        </Paper>
    )
}

export default ProfileHeader;