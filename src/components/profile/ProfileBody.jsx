import React from 'react';
// import Styles from '../../modules/Profile.module.css';
import PostCard from '../widgets/PostCard';
import { CircularLoader } from '../widgets/SpinnerLoading';
import CustomStyles from '../../modules/Profile.module.css';

import { firebaseLooper, postsDatabase } from '../../Firebase';

import { useRecoilValue, useRecoilState } from 'recoil';
import { postFetchedData, profileUserEditState } from '../../RecoilStore/RecoilAtom';



// Profile Body Components Function..
const ProfileBody = ({ isProfile }) => {
    const [postData, setPostData] = useRecoilState(postFetchedData);
    // Recoil State..
    const userInfo = useRecoilValue(profileUserEditState);

    // console.log(userInfo);

    // Useeffect hook of react application..
    React.useEffect(() => {
        const username = userInfo.username;
        fetchSpecificUserData(username);
    }, [userInfo.username]);


    // Fetching data and set to recoil specific user data..
    const fetchSpecificUserData = (username) => {
        postsDatabase.once('value')
            .then(snapData => {
                let getData = firebaseLooper(snapData);
                let wrappedData = [];

                for (let i = 0; i < getData.length; i++) {
                    if (getData[i].username === username) {
                        // console.log(getData[i])
                        wrappedData.push(getData[i]);
                    }
                }

                // console.log(wrappedData);
                if (wrappedData.length) {
                    setPostData(wrappedData);
                }
            });
    }


    // See the Accual Data here..
    // console.log(postData);


    // Rendering the Post here..
    const renderPost = (post) => (
        post.length ?
            post.map(item => (
                <div key={item.id}>
                    <PostCard
                        postData={{
                            id: item.id,
                            title: item.title,
                            image: item.image,
                            content: item.body,
                            date: item.time
                        }}
                        postAdminData={{
                            displayAdminName: item.displayAdminName,
                            adminAvatar: item.adminAvatar,
                            adminEmail: item.adminEmail,
                            lastSignIn: item.lastSignIn
                        }}
                        isProfile={isProfile}
                    />
                </div>
            ))
            :
            <CircularLoader customStyle={CustomStyles.postLoader} />
    );


    // returning statement of this component..
    return renderPost(postData);
}

export default ProfileBody;