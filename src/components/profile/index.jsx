import React from 'react';
import clsx from 'clsx';
import { Container, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { firebase ,postsDatabase } from '../../Firebase';

import moduleStyles from '../../modules/Profile.module.css';
import ProfileHeader from './ProfileHeader';
import MakePostSection from './ProfileMakePost';
import ProfileBody from './ProfileBody';

//Recoil..
import { useRecoilState } from 'recoil';
import { profileUserEditState } from '../../RecoilStore/RecoilAtom';


//Styles of classess..
const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
        marginTop: 10
    },
}));

//Profile of this component..
const Profile = (props) => {
    const classes = useStyles();

    // Recoil Hook..
    const [profileUserData, setProfileUserData] = useRecoilState(profileUserEditState);

    // React Hook..
    const [profileAction, setProfileAction] = React.useState({
        edit: false,
    });
    const [resultCondition, setResultCondition] = React.useState({
        loading: false,
        errorMsg: null
    });

    // See the Profile URL on get blank..
    // console.log(props.match.path.split('/')[1]);


    //Onchange Function for working with input...
    const editByClick = (event, condition) => {
        const customText = event.target.value;

        if (condition === 'profileName') {
            // setProfileData({...profileData, username: customText});
            setProfileUserData({ ...profileUserData, username: customText });

        }
    };

    //KeyPress Enter to Back to text changed..
    const keyPress = (event) => {
        const keyPress = event.key;

        if (keyPress === 'Enter') {
            setTimeout(() => {
                setProfileAction({...profileAction, edit: false});
            }, 200);
        }
    }

    //To Edit Input..
    const editBar = (text, label, condition) => (
        <TextField
            id="filled-helperText"
            label={label}
            defaultValue={text}
            variant="filled"
            onChange={(event) => editByClick(event, condition)}
            onKeyPress={(event) => keyPress(event)}
        />
    );


    // Submit Post To Firebase Database..
    const submitPost = ({ postTitle, postBody, postImage, ...postData }) => {


        console.log('=== DATA SUBMITTED TO SERVER ===' ,postData);

        let dataToSubmit = {};

        if(postData){
            setResultCondition({...resultCondition, loading: true});
            
            // Fetching Posts Data to Id of Posts...
            postsDatabase.orderByChild("id").limitToLast(1).once("value")
            .then(postSnap => {
                let postId = null;
                postSnap.forEach(childData => {
                    postId = childData.val().id;
                });

                // Make Update the Empty New Object with real Data..
                dataToSubmit['id'] = postId + 1;
                dataToSubmit['time'] = firebase.database.ServerValue.TIMESTAMP;
                dataToSubmit['title'] = postTitle;
                dataToSubmit['body'] = postBody;
                dataToSubmit['image'] = postImage;
                dataToSubmit['username'] = profileUserData.username;
                dataToSubmit['lastSignIn'] = postData.lastSignIn;
                dataToSubmit['displayAdminName'] = postData.displayName;
                dataToSubmit['adminAvatar'] = postData.adminAvatar;
                dataToSubmit['adminEmail'] = postData.adminEmail;

                // console.log(dataToSubmit);

                //Make post to database(Realtime Firebase)..
                postsDatabase.push(dataToSubmit)
                .then(snapData => {
                        // console.log('The Successfull ID is -->> ', snapData);
                        props.history.push(`/post/${snapData.key}`);
                })
                .catch(error => {
                    setResultCondition({...resultCondition, loading: false, errorMsg: error.message});
                    // console.log('ERR! when Make Post For Firebase -->> ', error.message);
                });
            });
        }
    }


    ///Components Return Statement...
    return (
        <Container className={clsx(classes.root, moduleStyles.profileContainer)}>
            <ProfileHeader
                {...profileUserData}
                {...profileAction}
                editBar={editBar}
                setEdit={setProfileAction}
                profileAction={profileAction}
                userInfo={props.userInfo}
            />
            <MakePostSection 
                {...profileUserData} 
                userInfo={props.userInfo} 
                submitPost={submitPost}
                loading={resultCondition.loading}
                resultMsg={resultCondition.errorMsg}
            />
            <ProfileBody 
                isProfile={props.match.path.split('/')[1] === 'profile' ? true : false} 
            />
        </Container>
    )
}

export default Profile;