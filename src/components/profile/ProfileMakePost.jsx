import React from 'react';
import { Paper, Grid, Modal, Backdrop, Fade, Button } from '@material-ui/core';
import { Send as SendIcon } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import customStyles from '../../modules/Profile.module.css';

import PostEditor from '../widgets/TextEditor';
import { CircularLoader } from '../widgets/SpinnerLoading';
import AlertMsg from '../widgets/AlertReport';
import ImageUploader from '../widgets/Uploader';

//Recoil..
import { useRecoilState } from 'recoil';
import { postState } from '../../RecoilStore/RecoilAtom';


// Styles Of material ui..
const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    modalPaper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid gray',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        outline: '0',
        width: '700px',
    },


    button: {
        margin: theme.spacing(1),
    },
    loadingStyle: {
        textAlign: 'center',
        position: 'relative',
        left: '45%',
        top: '20%'
    }
}));


// Componenets Function..
const ProfileMakePost = ({ userInfo, username, submitPost, loading, resultMsg }) => {
    const classes = useStyles();
    const [openModal, setOpenModal] = React.useState(false);
    const [postItems, setPostItems] = useRecoilState(postState);

    const { displayName, photoURL, metadata, email } = userInfo;

    // console.log('Post Image here --->>> ', postItems.postImage);
    // console.log('Post Value here --->>> ', postItems.postTitle);

    // showing MsgResult of Post Done Or Not..
    const showMsg = (isError) => (
        !isError && isError === null ?
            null
            : isError ?
                <AlertMsg severityType="error" alertText={isError} />
                :
                <AlertMsg severityType="success" alertText="Successfully Posted!" />
    )

    // Post Submit Button Or Loading after submit..
    const postSubmitButton = (isLoading) => (
        !isLoading ?
            <Button
                variant="contained"
                color="secondary"
                className={classes.button}
                endIcon={<SendIcon />}
                onClick={beforeSubmit}
            >
                post
            </Button>
            :
            <CircularLoader customStyle={classes.loadingStyle} />
    )

    // Some work before submit..
    const beforeSubmit = () => {
        let postData = {
            postTitle: postItems.postTitle,
            postBody: postItems.postBody,
            postImage: postItems.postImage,
            lastSignIn: metadata.lastSignInTime,
            displayName,
            adminAvatar: photoURL,
            adminEmail: email
        };
        submitPost({ ...postData });

        console.log(postData);
    }

    // console.log(metadata.lastSignInTime);


    //Components Return statement..
    return (
        <React.Fragment>
            {/*----- Paper of Post -----*/}
            <Paper className={customStyles.paper}>
                <Grid spacing={1}>
                    <div onClick={() => setOpenModal(true)}>
                        <input className={customStyles.postReadOnlyField} type="text" value="What's on your mind?" disabled />
                    </div>
                </Grid>
            </Paper>

            {/*----- Modal Of Post -----*/}
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={openModal}
                onClose={() => setOpenModal(false)}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{ timeout: 500, }}
            >
                <Fade in={openModal}>
                    <Paper className={classes.modalPaper}>
                        {/* Modal Body...*/}
                        {/*----- Modal Post Heading -----*/}
                        <div style={{ display: 'flex' }}>
                            <div>
                                <img className={customStyles.modalAvatar} src={photoURL ? photoURL : 'images/profile-avatar.jpg'} alt="No Avatar" />
                            </div>

                            <div style={{ marginLeft: '5px', marginTop: '-10px' }}>
                                <h3>{displayName ? displayName : username}</h3>
                            </div>
                        </div>

                        {/*----- Modal Post Area -----*/}
                        <div style={{ display: 'grid' }}>
                            {/* Post Title */}
                            <input
                                type="text"
                                placeholder="What's the title"
                                className={customStyles.postTitleStyle}
                                value={postItems.postTitle}
                                onChange={event => setPostItems({...postItems, postTitle: event.target.value})}
                            />

                            {/* Text Editor from widgets */}
                            <PostEditor
                                setValue={setPostItems}
                                editorPlaceholder="Write your post here.."
                                type="postBody"
                                value={postItems.postBody}
                            />

                            {/* File / Photo Uploading here */}
                            {/* <input
                                className={customStyles.photoUpload}
                                type="file"
                                value={postItems.postImage}
                                onChange={(event) => handleOnChange(event, 'image')}
                            /> */}
                            <ImageUploader customStyle={customStyles.imageUploader} />

                            {/*---- Post Button ----*/}
                            {postSubmitButton(loading)}

                            {/*---- result Msg ----*/}
                            {showMsg(resultMsg)}
                        </div>
                    </Paper>
                </Fade>
            </Modal>
        </React.Fragment>
    );
}

export default ProfileMakePost;