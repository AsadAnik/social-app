import React from 'react';
import { Spinner } from '../widgets/SpinnerLoading';
import PostView from '../widgets/PostCard';
// import PostCreate from '../profile/ProfileMakePost';
import CustomStyle from '../../modules/Home.module.css';

// Firebase..
import { postsDatabase, firebaseLooper } from '../../Firebase';
// Recoil..
import { useRecoilState, useRecoilValue } from 'recoil';
import { postFetchedData, profileUserEditState } from '../../RecoilStore/RecoilAtom';


// Home Components Function..
const Home = () => {
    // Recoil State Hook..
    const [postData, setPostData] = useRecoilState(postFetchedData);
    const profileUserData = useRecoilValue(profileUserEditState);
    const { appThemeColor } = profileUserData;

    //LifeCycle Alternative Hoookkk...
    React.useEffect(() => {
        getPostFetchedData();
    }, ['']);


    // Get Fetch data and set to RecoilStore..
    const getPostFetchedData = () => {
        postsDatabase.once('value')
            .then(postSnap => {
                const postData = firebaseLooper(postSnap);
                setPostData([...postData]);
            });
    }


    //Retuening the home items...
    const renderHomeItems = (data) => (
        data.map(item => (
            <div key={item.id} className={CustomStyle.postRoot}>
                <PostView
                    postData={{
                        id: item.id,
                        title: item.title,
                        image: item.image,
                        content: item.body,
                        date: item.time
                    }}
                    postAdminData={{
                        displayAdminName: item.displayAdminName,
                        adminAvatar: item.adminAvatar,
                        adminEmail: item.adminEmail,
                        lastSignIn: item.lastSignIn
                    }}
                    customStyles={CustomStyle.postViewSection}
                    isProfile={false}
                />
            </div>
        )
    ));


    //Rendering HomeData function..
    const renderHomeData = (data) => {
        if (data.length) {
            return (
                <React.Fragment>
                    {/* {renderCreatePost()} */}
                    {renderHomeItems(data)}
                </React.Fragment>
            );

        } else {
            return <Spinner size={50} color={appThemeColor} loaderCondition={true} />;
        }
    }

    //Returning Statement..
    return (
        <React.Fragment>
            {renderHomeData(postData)}
        </React.Fragment>
    )
}

export default Home;