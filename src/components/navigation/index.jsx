import React from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import {
    AppBar,
    Toolbar,
    CssBaseline,
    Typography,
    IconButton,
    Badge,
    Drawer,
    List,
    Divider,
    ListItem,
    ListItemIcon,
    ListItemText,
    Tooltip,
} from '@material-ui/core';
import {
    Mail as MailIcon,
    AccountCircle as AccountCircleIcon,
    FavoriteTwoTone as FavIcon,
    MoreVert as MoreIcon,
    Menu as MenuIcon,
    ChevronLeft as ChevronLeftIcon,
    ChevronRight as ChevronRightIcon,
    Home as HomeIcon,
    Settings as SettingIcon,
    AccountBox as ProfileIcon,
    Info as AboutIcon,
} from '@material-ui/icons';
import { Link } from 'react-router-dom';
import Logout from './logout';

// Recoil Store..
import { useRecoilValue } from 'recoil';
import { profileUserEditState } from '../../RecoilStore/RecoilAtom';

// import DrawerOpen from './drawer';

//Width Of Drawer..
const drawerWidth = 240;

// The Drawer Navigation and NavBar/AppBar Component..
const DrawerNav = ({ userInfo }) => {
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);

    // Recoil Store Hook..
    const themeColor = useRecoilValue(profileUserEditState);
    const { appThemeColor } = themeColor;


    // UseStyles Of Material UI...
    const useStyles = makeStyles((theme) => ({
        root: {
            display: 'flex',
        },
        appBar: {
            zIndex: theme.zIndex.drawer + 1,
            transition: theme.transitions.create(['width', 'margin'], {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.leavingScreen,
            }),

            background: appThemeColor
        },
        appBarShift: {
            marginLeft: drawerWidth,
            width: `calc(100% - ${drawerWidth}px)`,
            transition: theme.transitions.create(['width', 'margin'], {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.enteringScreen,
            }),
        },
        menuButton: {
            marginRight: 36,
        },
        hide: {
            display: 'none',
        },

        toolbar: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-end',
            padding: theme.spacing(0, 1),
            // necessary for content to be below app bar
            ...theme.mixins.toolbar,
        },
        drawer: {
            width: drawerWidth,
            flexShrink: 0,
            whiteSpace: 'nowrap',
        },
        drawerOpen: {
            width: drawerWidth,
            transition: theme.transitions.create('width', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.enteringScreen,
            }),
            background: appThemeColor,
            color: '#e6e6e6'
        },

        drawerClose: {
            transition: theme.transitions.create('width', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.leavingScreen,
            }),
            overflowX: 'hidden',
            width: theme.spacing(7) + 1,
            [theme.breakpoints.up('sm')]: {
                width: theme.spacing(9) + 1,
            },
            background: appThemeColor,
            color: '#e6e6e6'
        },
        content: {
            flexGrow: 1,
            padding: theme.spacing(3),
        },
        title: {
            display: 'none',
            [theme.breakpoints.up('sm')]: {
                display: 'block',
            },
        },
        grow: {
            flexGrow: 1,
        },
        sectionDesktop: {
            display: 'none',
            [theme.breakpoints.up('md')]: {
                display: 'flex',
            },
        },
        sectionMobile: {
            display: 'flex',
            [theme.breakpoints.up('md')]: {
                display: 'none',
            },
        },
        proAvatar: {
            width: '30px',
            height: '30px',
            borderRadius: '100%',
            border: '2px solid lightgray'
        }
    }));
    const classes = useStyles();


    //showing the profile avatar form userInfo...
    const showProfileAvatar = (profileAvatar) => (
        profileAvatar ?
            <img src={profileAvatar} className={classes.proAvatar} alt={'No Profile'} />
            :
            <AccountCircleIcon />
    );

    //handle drawer Open..
    const handleDrawerOpen = () => {
        setOpen(true);
    };

    //hangle drawer close..
    const handleDrawerClose = () => {
        setOpen(false);
    };

    // console.log('Profile UserInfo ---- ', userInfo);

    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, {
                            [classes.hide]: open,
                        })}
                    >
                        <MenuIcon color={'white'} />
                    </IconButton>

                    {/*--------- Navigation Text ----------*/}
                    <Typography className={classes.title} variant={'h6'} noWrap>Social Community</Typography>

                    {/*--------- Navigation right-sides Icons to control -----------*/}
                    <div className={classes.grow} />
                    <div className={classes.sectionDesktop}>
                        {/*---- Mail Icon ----*/}
                        <Tooltip title="All Mails">
                            <IconButton aria-label="show 4 new mails" color="inherit">
                                <Badge badgeContent={4} color="secondary">
                                    <MailIcon />
                                </Badge>
                            </IconButton>
                        </Tooltip>

                        {/*----- Notification Icon -----*/}
                        <Tooltip title="Total Love">
                            <IconButton aria-label="show 17 new notifications" color="inherit">
                                <Badge badgeContent={17} color="secondary">
                                    <FavIcon />
                                </Badge>
                            </IconButton>
                        </Tooltip>

                        {/*----- AccountCircle Icon -----*/}
                        <Tooltip title="Profile Picture">
                            <IconButton
                                edge="end"
                                aria-label="account of current user"
                                aria-controls={''}
                                aria-haspopup="true"
                                color="inherit"
                                component={ Link }
                                to="/profile"
                            >
                                {showProfileAvatar(userInfo.photoURL)}
                            </IconButton>
                        </Tooltip>
                    </div>

                    {/*------------- Mobile Section of Navigation -----------*/}
                    <div className={classes.sectionMobile}>
                        <IconButton
                            aria-label="show more"
                            aria-controls={''}
                            aria-haspopup="true"
                            color="inherit"
                        >
                            <MoreIcon />
                        </IconButton>
                    </div>
                </Toolbar>
            </AppBar>

            {/*------------ Drawer Section -----------*/}
            {/* <DrawerOpen 
                handleDrawerClose={handleDrawerClose} 
                open={open} 
            /> */}

            <Drawer
                variant="permanent"
                className={clsx(classes.drawer, {
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                })}
                classes={{
                    paper: clsx({
                        [classes.drawerOpen]: open,
                        [classes.drawerClose]: !open,
                    }),
                }}
            >
                <div className={classes.toolbar}>
                    <IconButton onClick={handleDrawerClose} color="inherit">
                        {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                    </IconButton>
                </div>

                <Divider />

                <List>
                    <ListItem button component={Link} to={'/'} exact >
                        <ListItemIcon style={{ color: 'white' }}>
                            <HomeIcon />
                        </ListItemIcon>
                        <ListItemText primary={'Home'} />
                    </ListItem>

                    <ListItem button component={Link} to={'/profile'}>
                        <ListItemIcon style={{ color: 'white' }}>
                            <ProfileIcon />
                        </ListItemIcon>
                        <ListItemText primary={'Profile'} />
                    </ListItem>

                    <ListItem button component={Link} to={'/settings'}>
                        <ListItemIcon style={{ color: 'white' }}>
                            <SettingIcon />
                        </ListItemIcon>
                        <ListItemText primary={'Settings'} />
                    </ListItem>

                    <ListItem button component={Link} to={'/about'}>
                        <ListItemIcon style={{ color: 'white' }}>
                            <AboutIcon />
                        </ListItemIcon>
                        <ListItemText primary={'About'} />
                    </ListItem>

                    {/* Logout Control Button */}
                    <Logout />

                </List>
                {/* <Divider /> */}
                {/* <List>
                    {['All mail', 'Trash', 'Spam'].map((text, index) => (
                        <ListItem button key={text}>
                            <ListItemIcon style={{color: 'white'}}>{index % 2 === 0 ? <ProfileIcon /> : <MailIcon />}</ListItemIcon>
                            <ListItemText primary={text} />
                        </ListItem>
                    ))}
                </List> */}
            </Drawer>

        </div>
    );
}

export default DrawerNav;