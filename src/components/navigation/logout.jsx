import React from 'react';
import { firebase } from '../../Firebase';
import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { ExitToApp as LogoutIcon } from '@material-ui/icons';

const Logout = (props) => {

    ///Firebase Logout System...
    const logoutAuth = () => {
        firebase.auth().signOut()
            .then(() => {
                props.history.push('/');
            })
            .catch(error => {
                console.log("Logout ERROR! -->> ", error.message);
            });
    }

    //Return Statement..
    return (
        <ListItem button onClick={logoutAuth} >
            <ListItemIcon style={{ color: 'white' }}>
                <LogoutIcon />
            </ListItemIcon>
            <ListItemText primary={'Logout'} />
        </ListItem>
    )
}

export default Logout;