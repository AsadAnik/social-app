import React from 'react';
import { Paper, CardHeader, Avatar } from '@material-ui/core';
import { Favorite } from '@material-ui/icons';
import customStyle from '../../modules/Post.module.css';
import PostMomentTime from '../widgets/MomentTime';


// Post Components Function...
const Post = ({ postData, postAdminData }) => {

    return (
        <>
            {/* The Title Of this Post */}
            <Paper className={customStyle.postPaper}>
                <CardHeader
                    avatar={
                        <Avatar aria-label="recipe">
                            <img src={postAdminData.avatar} alt="AdminURL" />
                        </Avatar>
                    }
                    title={`(${postAdminData.displayName}) ${postData.title}`}
                    subheader={<PostMomentTime time={postData.time} />}
                />
            </Paper>

            {/* Post Cover Image */}
            <Paper className={customStyle.postPaper}>
                <img
                    src={postData.image}
                    alt="PostData"
                    width="100%"
                />

                {/* Content LIke/Love Amount */}
                <div style={{ display: 'inline-flex', marginTop: '0.2rem' }}>
                    <span>
                        <Favorite style={{ color: 'red', marginRight: '0.2rem' }} />
                    </span>
                    <span style={{ marginTop: '0.1rem', fontWeight: 'bold' }}>1K</span>
                </div>
            </Paper>

            {/* Post Content */}
            <Paper className={customStyle.postPaper} style={{ padding: '1rem' }}>

                {/* Post Content */}
                <p
                    dangerouslySetInnerHTML={{
                        __html: postData.content
                    }}
                >
                </p>

                {/* Post Admin Email */}
                <p style={{ color: 'gray' }}>Email: {postAdminData.email}</p>
            </Paper>
        </>
    );
}

export default Post;