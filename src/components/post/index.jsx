import React from 'react';
// import { fetchSpecificData } from '../../API/apiService';
import { firebaseDB } from '../../Firebase';
import { CircularLoader } from '../widgets/SpinnerLoading';
import CustomStyle from '../../modules/Post.module.css';
import Post from './Post';

// Post components function...
const PostRoot = (props) => {
    const [postData, setPostData] = React.useState([]);
    const postId = props.match.params.id;


    React.useEffect(() => {
        setTimeout(async () => {
            // let fetchData = await fetchSpecificData(postId);
            // let fetchAccualData = fetchData[0];
            // setPostData({ fetchAccualData });

            // Now for Fetch Firebase to Specific Post Data..
            await fetchFromFirebase(postId);

        }, 1500);
    }, [postId]);


    // Fetch From Firebase Database..
    const fetchFromFirebase = async (id) => {
        firebaseDB.ref(`posts/${id}`).once('value')
            .then(snapShot => {
                const snapPostData = snapShot.val();
                setPostData({ ...snapPostData });
            });
    };


    // Rendering the Posts from loading or data...
    const renderPost = (data) => (
        !data.id ?
            (
                <CircularLoader customStyle={CustomStyle.postLoader} />
            )
            :
            (
                <Post
                    postData={{
                        id: data.id,
                        title: data.title,
                        image: data.image,
                        content: data.body,
                        time: data.time
                    }}
                    postAdminData={{
                        displayName: data.displayAdminName,
                        email: data.adminEmail,
                        avatar: data.adminAvatar
                    }}
                />
            )
    )

    // Render Function of  this component..
    // return renderPost(getData);
    return renderPost(postData);
};

export default PostRoot;