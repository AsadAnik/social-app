// Atom is the Data storing system here..
import { atom } from 'recoil';

// Profile Edited to user showing..
const profileUserEditState = atom({
    key: 'userState',
    default: {
        username: 'New State Data',
        appThemeColor: 'black'
    }
});

// Store postItems Globally..
const postState = atom({
    key: 'postState',
    default: {
        postTitle: '',
        postBody: '',
        postImage: ''
    }
});

// Fetching Storage data of posts Database..
const postFetchedData = atom({
    key: 'postFetchedData',
    default: []
});


// exporting data combos..
export {
    profileUserEditState,
    postState,
    postFetchedData,
    // userInfoState
};