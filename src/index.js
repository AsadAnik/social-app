import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Auth from './authentication';
import { firebase } from './Firebase';
import { RecoilRoot } from 'recoil';


// Checking Authentication then returing to home page or login page..
const applicationRender = (isUser, userInfo) => {
    if (!isUser) {
        return <Auth /> ;
    } else {
        // console.log(userInfo);
        return (
            <RecoilRoot>
                <App userInfo = { userInfo } />
            </RecoilRoot>
        );
    }
}

//When Change the Auth system on application then check if once..
firebase.auth().onAuthStateChanged((user) => {
    // console.log(user);
    ReactDOM.render( 
        <React.StrictMode> { applicationRender(user ? true : false, user) } </React.StrictMode>,
        document.getElementById('root')
    );
});