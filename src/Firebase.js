import firebase from 'firebase/app';
import "firebase/auth";
import "firebase/database";
import "firebase/storage";

// Firebase Key and connectivity with application..
const firebaseConfig = {
    apiKey: "AIzaSyDsfbuGgjBqaINqq5qkqCNJv8PdAOUV8og",
    authDomain: "social-fd9ab.firebaseapp.com",
    projectId: "social-fd9ab",
    storageBucket: "social-fd9ab.appspot.com",
    messagingSenderId: "166452509466",
    appId: "1:166452509466:web:90476256396cd78e4a0aa3",
    measurementId: "G-9EBW1PR69N"
};

firebase.initializeApp(firebaseConfig);

//The Auth Providers system from firebase..
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
const facebookAuthProvider = new firebase.auth.FacebookAuthProvider();


/// Firebase Database (RealTime)...
const firebaseDB = firebase.database();

// posts table of database..
const postsDatabase = firebaseDB.ref('posts');

// users table of database..
const usersDatabase = firebaseDB.ref('users');


// Firebase Looper to multiple data..
const firebaseLooper = (data) => {
    let newData = [];

    data.forEach(snapData => {
        newData.push({
            ...snapData.val(),
            id: snapData.key
        });
    });

    return newData;
}

// postsDatabase.push({
//     id: 1,
//     title: 'Title Of Post',
//     body: 'Body Of Post',
//     image: '1.jpg'
// });


// usersDatabase.push({
//     id: 0,
//     username: 'Asad Anik',
//     bio: 'I \'m crazy coder!'
// });


export {
    firebase,
    firebaseDB,
    googleAuthProvider,
    facebookAuthProvider,
    postsDatabase,
    firebaseLooper,
    usersDatabase
}