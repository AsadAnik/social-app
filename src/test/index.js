import React from 'react';
import { Route, Redirect } from 'react-router-dom';

console.log('public and private route system here!');

function PrivateRoute() {
    return 'I am Private';
}

function PublicRoute({ userInfo ,component: Comp, ...rest }) {
    try {
        let RouteReturn = (
            <Route
                {...rest}
                component={(props) => (
                    !userInfo ? <Redirect to='/' /> : <Comp {...props} userInfo={userInfo} />
                )}
            />
        );

        if(!RouteReturn){
            return "ERR! Check On Test";
        }
        return RouteReturn;

    } catch (error) {
        console.log('This is ERR! Block Of Test :::: ' ,error);
    }
}

export {
    PrivateRoute,
    PublicRoute
};