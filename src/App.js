import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Routes from './route/Routes';
import './modules/App.module.css';
import { firstUserStore } from './users.config';

// Recoil Store needed..
import { useRecoilValue } from 'recoil';
import { profileUserEditState } from './RecoilStore/RecoilAtom';

// App Components Function..
function App(props) {
  const { displayName, email, metadata, emailVerified, photoURL } = props.userInfo;

  // props checking here..
  // console.log('checking for testing == ', props);

  // Recoil Hook of value only..
  const themeColor = useRecoilValue(profileUserEditState);
  const { appThemeColor } = themeColor;


  // React Hook...
  React.useEffect(() => {
    // const username = makeUserDataSet();
    // firstUserStore(username);

    // If this is google User so run this code..
    if (displayName && emailVerified) {
      // const username = makeUserDataSet();
      firstUserStore({ displayName, email, metadata, emailVerified, photoURL, appThemeColor });
    }
  }, [' ']);  



  // Returning Statement..
  return (
    <React.Fragment>
      <Router>
        <Routes {...props} />
      </Router>
    </React.Fragment>
  );
}

export default App;
