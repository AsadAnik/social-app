import React from 'react';
import { firebase, googleAuthProvider, facebookAuthProvider } from '../../Firebase';

import { Button } from '@material-ui/core';
import { Facebook as FacebookIcon, Fingerprint as GoIcon } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';

import AlertReport from '../../components/widgets/AlertReport';


//Components Styles..
const styles = makeStyles(() => ({
    socialBtn: {
        width: '100%',
        marginTop: '1rem'
    },
    authMsgReport: {
        marginTop: '5rem'
    }
}));


//AuthProviders Component..
const ProvidersAuth = (props) => {
    const classes = styles();
    const [data, setData] = React.useState({
        authCondition: null,
        authSuccessMsg: '',
        authErrorMsg: ''
    });

    // console.log('----------- Props ------', props);

    //Google Auth Login..
    const googleClick = () => {
        firebase.auth().signInWithPopup(googleAuthProvider)
            .then(() => {
                setData({ ...data, authCondition: true, authSuccessMsg: 'Google Login Successful!' });
                props.history.push('/');
            })
            .catch(error => {
                setData({ ...data, authCondition: false, authErrorMsg: error.message });
            });
    }

    //Facebook Auth Login..
    const facebookClick = () => {
        firebase.auth().signInWithPopup(facebookAuthProvider)
            .then(() => {
                setData({ ...data, authCondition: true, authSuccessMsg: 'Facebook Login Successful!' });
                props.history.push('/');
            })
            .catch(error => {
                setData({ ...data, authCondition: false, authErrorMsg: error.message });
            });
    }


    //Return statement..
    return (
        <>
            {/* Google Auth Provider Btn */}
            <Button
                variant="contained"
                color="secondary"
                size="large"
                className={classes.socialBtn}
                startIcon={<GoIcon />}
                onClick={googleClick}
            >
                Google Login
                </Button>

            {/* Facebook Auth Provider Btn */}
            <Button
                variant="contained"
                style={{ background: 'royalblue', color: 'white' }}
                size="large"
                className={classes.socialBtn}
                startIcon={<FacebookIcon />}
                onClick={facebookClick}
            >
                Facebook Login
                </Button>

            {/* Error Message & Success Message Showing */}
            { !data.authCondition && data.authCondition !== null ?
                <AlertReport
                    severityType={'error'}
                    alertText={data.authErrorMsg}
                    customStyle={classes.authMsgReport}
                />

             : data.authCondition !== null && data.authCondition &&
                <AlertReport
                    severityType={'success'}
                    alertText={data.authSuccessMsg}
                    customStyle={classes.authMsgReport}
                />}
        </>
    )
}

export default ProvidersAuth;