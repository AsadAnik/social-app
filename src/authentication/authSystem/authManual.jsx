import React from 'react';
import { firebase } from '../../Firebase';

import {
    TextField,
    Button,
    InputAdornment,
    IconButton,
    OutlinedInput,
    InputLabel,
    FormControl,
    Grid,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Send as SubmitIcon, Visibility as EyeOn, VisibilityOff as EyeOff } from '@material-ui/icons';

import registerBg from '../images/authOne.jpg';
import ProvidersAuth from './authProvider';
import AlertReport from '../../components/widgets/AlertReport';


//Components Styles..
const styles = makeStyles(() => ({
    root: {
        height: '100vh',
        backgroundImage: `url(${registerBg})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundAttachment: 'fixed',
        backgroundPosition: 'center'
    },

    rootRight: {
        // position: 'absolute',
        backgroundColor: 'white',
        float: 'right',
        width: '40%',
        height: '100%',
        padding: '1rem',

    },
    inputBar: {
        width: '100%',
        marginBottom: '1rem',
    },
    submitBtn: {
        width: '100%',
        marginBottom: '1.5rem'
    },
    customLabel: {
        margin: 0,
        marginLeft: '1rem',
        padding: 0,
        marginTop: '-0.30rem',
        zIndex: 1
    },
    authMsgReport: {
        marginTop: '5rem'
    }
}));


//Components function..
const AuthSystem = (props) => {
    const classes = styles();
    const [values, setValues] = React.useState({
        email: '',
        password: '',
        errorMsg: '',
        formReady: false,
        emailCondition: true,
        passCondition: true,
        formCondition: '',
        showPassword: false,
        authErrorMsg: ''
    });


    //Eye Control for Password Showing..
    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    //custom validation function..
    const validatingSystem = ({ useremail: uEmail, userpassword: uPassword }) => {
        if (uEmail === '') {
            setValues({ ...values, emailCondition: false, errorMsg: 'Empty Field!', formReady: false });

        } else if (uPassword === '') {
            setValues({ ...values, passCondition: false, errorMsg: 'Empty Field!', formReady: false });

        } else {
            setValues({ ...values, passCondition: true, errorMsg: "", formReady: true, emailCondition: true });
        }
    };

    //Firebase Data parse and validate this..
    const parseToFirebase = ({ email, password }, userType) => {
        if (!userType) {///Registering the new User if userType False Register Btn click...
            firebase.auth().createUserWithEmailAndPassword(email, password)
                .then(() => {
                    setValues({...values, formCondition: 'Successfully Registered User!'});
                    props.history.push('/');
                })
                .catch(error => {
                    setValues({ ...values, authErrorMsg: error.message });
                });

        } else {///Login the User if userType True Login Btn click...
            firebase.auth().signInWithEmailAndPassword(email, password)
                .then(() => {
                    setValues({...values, formCondition: 'Logged In Successfully Done!'});
                    props.history.push('/');
                })
                .catch(error => {
                    setValues({ ...values, authErrorMsg: error.message });
                });
        }
    };


    //submit form..
    const submitForm = (event, userType) => {
        event.preventDefault();
        // console.log({email, password});

        const formValues = {
            useremail: values.email,
            userpassword: values.password
        }

        validatingSystem({ ...formValues });

        //userData..
        let userData = {};

        ///Form Submition...
        if (!values.formReady) {
            console.log('Error with form submittion!');

        } else if (values.emailCondition && values.passCondition && values.formReady) {
            userData = {
                email: values.email,
                password: values.password
            }

            parseToFirebase({ ...userData }, userType);
        }

    }

    //input textarea value setup..
    const onValueChange = (event, type) => {
        if (type === 'email') {
            setValues({ ...values, email: event.target.value });
        }

        if (type === 'password') {
            setValues({ ...values, password: event.target.value });
        }
    }


    //Return statement of component..
    return (
        <div className={classes.root}>
            <form className={classes.rootRight} onSubmit={(e) => submitForm(e, null)}>
                <label>
                    <h3 style={{ textAlign: 'center' }}>Authentication Here</h3>
                </label>

                {/*------ Login / Register Auth System Firebase ------*/}
                {/*---- Email Field ----*/}
                <TextField
                    className={classes.inputBar}
                    id="outlined-basic"
                    variant="outlined"
                    label={!values.emailCondition ? values.errorMsg : "Email"}
                    value={values.email}
                    onChange={(event) => onValueChange(event, 'email')}
                    error={!values.emailCondition ? true : false}
                    helperText={!values.emailCondition ? "Incorrect Entry" : ""}
                />

                {/*---- Password Field With Eye Control ----*/}
                <FormControl className={classes.inputBar}>
                    <InputLabel htmlFor="outlined-adornment-password" className={classes.customLabel}>
                        {!values.passCondition ? <span style={{ color: 'red' }}>{values.errorMsg}</span> : "Password"}
                    </InputLabel>

                    <OutlinedInput
                        id="outlined-adornment-password"
                        variant="outlined"
                        type={values.showPassword ? 'text' : 'password'}
                        autoComplete="current-password"
                        value={values.password}
                        onChange={(event) => onValueChange(event, 'password')}
                        error={!values.passCondition ? true : false}
                        labelWidth={90}
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleClickShowPassword}
                                    onMouseDown={handleMouseDownPassword}
                                >
                                    {values.showPassword ? <EyeOn /> : <EyeOff />}
                                </IconButton>
                            </InputAdornment>
                        }
                    />
                </FormControl>


                {/*---- Submit Form Button ----*/}
                <Grid container spacing={1}>
                    <Grid item xs={6}>
                        <Button
                            variant="contained"
                            color="primary"
                            size="large"
                            className={classes.submitBtn}
                            endIcon={<SubmitIcon />}
                            onClick={(e) => submitForm(e, true)}
                        >
                            Login
                    </Button>
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            variant="contained"
                            color="primary"
                            size="large"
                            className={classes.submitBtn}
                            endIcon={<SubmitIcon />}
                            onClick={(e) => submitForm(e, false)}
                        >
                            Register
                    </Button>
                    </Grid>
                </Grid>
                <hr />

                {/*-------- The Facebook & Google Authentication API -------*/}
                <ProvidersAuth />


                {/* Showing the Error Message when data form Submitted to Firebase */}
                { values.authErrorMsg && 
                    <AlertReport 
                        severityType={'error'} 
                        alertText={values.authErrorMsg} 
                        customStyle={classes.authMsgReport} 
                    /> }
                
                {/* Showing Success Message */}
                { values.formCondition && 
                    <AlertReport
                        severityType={'success'}
                        alertText={values.formCondition}
                        customStyle={!values.authErrorMsg ? classes.authMsgReport : null}
                    />
                }


            </form>

        </div>
    )
}

export default AuthSystem;