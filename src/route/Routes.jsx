import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Layout from '../HOC/layout';

///The Routes...
import Home from '../components/home';
import Profile from '../components/profile';
import Settings from '../components/settings';
import About from '../components/about';
import Post from '../components/post';

//Test the Public & Private Custom Route here..
// import { PublicRoute } from '../test';

//Custom Route..
import CustomRoute from './CustomRoute';


const Routes = (props) => {
    // Returning statement of component..
    return (
        <Layout userInfo={props.userInfo}>
            <Switch>
                <Route path='/' exact component={Home} />
                <CustomRoute {...props} path='/settings' component={Settings} />
                <Route path='/about' component={About} />
                {/* <Route {...props} path='/profile' component={Profile} /> */}
                <CustomRoute {...props} path='/post/:id' component={Post} />

                <CustomRoute {...props} path='/profile' component={Profile}  />
            </Switch>
        </Layout>
    )
}

export default Routes;