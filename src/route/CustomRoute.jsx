import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const CustomRoute = ({ userInfo, component: Comp, ...rest }) => {
    return (
        <Route {...rest}
            component={(props) => (
                !userInfo ? <Redirect to='/' /> : <Comp {...props} userInfo={userInfo} />
            )}
        />
    )
}

export default CustomRoute;