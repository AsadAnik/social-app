import React from 'react';
import { Container } from '@material-ui/core';
import Navigation from '../components/navigation';

const Layout = (props) => {
    return (
        <React.Fragment>
            <Navigation userInfo={props.userInfo} />
            <Container maxWidth={'lg'}>
                <div style={{marginTop: '5rem', marginLeft: '5rem'}}>
                    {props.children}
                </div>
            </Container>
        </React.Fragment>
    )
}

export default Layout;