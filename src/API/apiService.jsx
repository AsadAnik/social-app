const allDataForHome = async() => {
    const db = [];

    await fetch('https://jsonplaceholder.typicode.com/posts')
        .then(res => res.json())
        .then(res => {
            db.push(res);
        })
        .catch(error => console.log('Error API Fetching Got ERR!: ', error));

        if(db.length > 0){
            return db;
        }
}

const fetchSpecificData = async(postId) => {
    const db = [];

    await fetch(`https://jsonplaceholder.typicode.com/posts?id=${postId}`)
        .then(res => res.json())
        .then(resData => {
            db.push(resData);
        })
        .catch(error => console.log('Error API Fetching Specific Data Got ERR! ', error));

        if(db.length > 0){
            return db;
        }
}

export {
    allDataForHome,
    fetchSpecificData
};