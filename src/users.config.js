import { usersDatabase } from './Firebase';


  // Put userData into Recoil..
  const firstUserStore = (userData) => {
    let username = [];
    let { displayName, email, metadata, emailVerified, photoURL } = userData;

    const makedUsername = userNameMaker(displayName);

    // Fetch users all for username..
    usersDatabase.orderByChild("id").once("value")
      .then(snapShot => {

        snapShot.forEach(childAgain => {
          username.push(childAgain.val().username);
        });

        // Fetch users to get id of users database..
        usersDatabase.orderByChild("id").limitToLast(1).once("value")
          .then(snapAgain => {
            let userId = null;
            snapAgain.forEach(childData => {
              userId = childData.val().id;
            });

            let previousUser = false;
            // Loop throw to checking the previous user...
            username.forEach(users => {
              if (users === makedUsername) {
                previousUser = true;
              }
            });

            // if not user is old or previous user then create one..
            if (!previousUser) {
              usersDatabase.push({
                id: userId + 1,
                username: makedUsername,
                email,
                displayName,
                creationTime: metadata.creationTime,
                lastSignInTime: metadata.lastSignInTime,
                emailVerified,
                userAvatar: photoURL,
              })
                .catch(error => console.log(error.message));
            }

          });

      })
      .catch(error => console.log(error.message));
  };

  // Make displayName to username...
//   const makeUserDataSet = () => {
//     const makedUsername = userNameMaker(displayName);
//     return makedUsername;
//   };

  // Username Maker from displayName Backend(Firebase) users data..
  const userNameMaker = (normalName) => {
    normalName = normalName.toLowerCase().split(' ');
    normalName = normalName[0].concat(normalName[1]);
    return normalName;
  };


export {
    firstUserStore,
};